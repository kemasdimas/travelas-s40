/**
 * TODO:
 * 
 * 
 */
var Travelas = (function() {
	var DEBUG = false;	
	var HERE_APP_ID = 'qjaPkSRiWBu8BDbYkUMZ';
	var HERE_APP_TOKEN = 'WBc0MlQGjahSzSSSUFHvFw';
	var SERVER_ERROR_STRING = string_error_server_problem;
	var MY_TWH = '196903';
	var TRANSITION_FUNCTION = "mwl.scrollTo('#ui-header'); mwl.switchClass('#progress-anim', 'progress-hide', 'progress-show'); mwl.stopTimer(); mwl.removeClass('#additional-menu', 'height-menu-shown'); ";
	var shortAppUrl = 'bit.ly/travelas40';
	var ongkirAdsUrl = 'http://api.ongkir.info/nokia/store_url';
	
	var mapZoom = 12;
	var mapBaseUrl = 'http://m.nok.it/?app_id=' + HERE_APP_ID + '&app_code=' + HERE_APP_TOKEN + '&nord&h=240';
	var tiketURL = "http://api.tiket.com/";
	var tiketKey = "77c7104d1ada607c4fef83e30df6d4ca";
	var storedToken = "";
	var flightSearchResult = null;
	var defaultSort = 'priceasc';
	var storage = {
		departDateObject: null,
		returnDateObject: null,
		departFlightObject: null,
		returnFlightObject: null,
		geolocation: null,
		nearestAirportCode: null
	};
	
	var tiketTokenUrl = tiketURL + 'apiv1/payexpress?output=json&method=getToken&secretkey=' + tiketKey;
	var tiketSearchUrl = tiketURL + 'search/flight?output=json&';
	var tiketAirportUrl = tiketURL + 'flight_api/all_airport?output=json&';
	var tiketPromoUrl = tiketURL + 'general_api/get_flight_promo?output=json&';
	var tiketRedirOrderUrl = 'https://m.tiket.com/order/add/flight?twh=' + MY_TWH;
	var tiketCheckUpdateUrl = tiketURL + 'ajax/mCheckFlightUpdated?output=json&';
	
	// Ads related param
	var naxAppId = 'WGchat_travelass40_Nokia';
	var naxContainerId = 'ads-placeholder';
	var naxInlineContainerId = 'ads-inline';
	
	//This is a hack for the location
	function _locationSuccess(position) {
		var latitude = position.coords.latitude;
		var longitude = position.coords.longitude;
		
		storage.geolocation = {
			'latitude': latitude,
			'longitude': longitude
		};
		
		// TODO: Iterate all airport location and find the nearest airport
		var shortestLength = 999999;
		var nearestAirportCode = null;
		for (var key in AIRPORT_LOCATIONS) {
			var airportLocation = AIRPORT_LOCATIONS[key];
			
			var calcLength = Travelas.calculateDistance(storage.geolocation.latitude, storage.geolocation.longitude, airportLocation.lat, airportLocation['long']);
			if (calcLength < shortestLength) {
				shortestLength = calcLength;
				storage.nearestAirportCode = key;
			}
		}
		
		_init();
	};

	function _locationFailed() {
		storage.geolocation = null;
		storage.nearestAirportCode = null;
		
		_init();
	};
	
	function _initLocation() {
		if (window.navigator && window.navigator.geolocation) {
			window.navigator.geolocation.getCurrentPosition(_locationSuccess, _locationFailed);
		} else {
			_init();
		}

		_translateElements('string_location_load', string_location_load);
	};
	
	function _init() {
		// hide the location load elem
		mwl.removeClass('#main-content-wrapper', 'hide');
		mwl.addClass('#location-load', 'hide');
		
		_getToken();
		_getPromo();
		
		_initiateSearchForm();
		
		// init search form value with previously inputted value
		var prevDeparture = _getPreference('departure', '');
		if (storage.nearestAirportCode != null) {
			prevDeparture = storage.nearestAirportCode;
		}
		
		var prevArrival = _getPreference('arrival', '');
		var prevAdult = _getPreference('adult', 1);
		var prevChild = _getPreference('child', 0);
		var prevInfant = _getPreference('infant', 0);
		
		document.getElementById('departure-airport').value = prevDeparture;
		document.getElementById('arrival-airport').value = prevArrival;
		document.getElementById('psg-adult').value = prevAdult;
		document.getElementById('psg-child').value = prevChild;
		document.getElementById('psg-infant').value = prevInfant;
		mapZoom = _getPreference('zoom', 12);
		defaultSort = _getPreference('sort', 'priceasc');
	
		// renaming string of existing elements
		_translateElements('string_promo_price_change', string_promo_price_change);
		_translateElements('string_book_flight', string_book_flight);
		_translateElements('string_back_to_top', string_back_to_top);
		_translateElements('string_loading_progress', string_loading_progress);
		_translateElements('string_skip_ads', string_skip_ads);
		_translateElements('string_tab_search', string_tab_search);
		_translateElements('string_tab_promo', string_tab_promo);
		_translateElements('string_round_trip_flight', string_round_trip_flight);
		_translateElements('string_departure_date', string_departure_date);
		_translateElements('string_return_date', string_return_date);
		_translateElements('string_day', string_day);
		_translateElements('string_month', string_month);
		_translateElements('string_year', string_year);
		_translateElements('string_passengers', string_passengers);
		_translateElements('string_adult', string_adult);
		_translateElements('string_child', string_child);
		_translateElements('string_infant', string_infant);
		_translateElements('string_yrs', string_yrs);
		_translateElements('string_search_flights', string_search_flights);
		_translateElements('string_zoom_in_map', string_zoom_in_map);
		_translateElements('string_zoom_out_map', string_zoom_out_map);
		_translateElements('string_customer_service', string_customer_service);
		_translateElements('string_about', string_about);
		_translateElements('string_back_to_main', string_back_to_main);
		_translateElements('string_contact_call', string_contact_call);
		_translateElements('string_cs_parag_1', string_cs_parag_1);
		_translateElements('string_about_parag_1', string_about_parag_1);
		_translateElements('string_about_parag_2', string_about_parag_2);
	};
	
	function _translateElements(elementName, translateString) {
		var elements = document.getElementsByName(elementName);
		var elementsCount = elements.length;
		
		for (var i = 0; i < elementsCount; i++) {
			var elem = elements.item(i);
			
			elem.innerHTML = translateString;
		}
	}
	
	function _showAds() {
//		if (inneractive) {
//			mwl.hide('#all-content-container');
//			mwl.switchClass('#ads-container', 'hide', 'show');
//			
//			inneractive.parameters.divID = naxContainerId;
//			inneractive.parameters.appID = naxAppId;
//			
//			inneractive.displayInneractiveAd();
//		}
	}
	
	function _showInlineAds(adsContainerId) {
		
		var adsContainer = document.getElementById(adsContainerId);
		adsContainer.innerHTML = '';
		
		// Check from ongkir.info server first, whether it returns 
		// ongkir.info nokia store URL
		// uncomment in api_nokia.php file in api.ongkir.info server!
		var ongkirResponse = _ajaxWrapper(ongkirAdsUrl);
		if (ongkirResponse.status == 200 && ongkirResponse.body != '') {
			var adsWrapper = document.createElement('a');
			adsContainer.appendChild(adsWrapper);
			adsWrapper.setAttribute('href', ongkirResponse.body);
			
			adsWrapper.innerHTML = '<img style="margin-top: 10px;" src="./travelan/img/ongkirads.jpg" />';
		} else {
			if (inneractive) {
				inneractive.parameters.keywords = 'travel,flight,ticket,tiket,pesawat,jalan-jalan,terbang';
				inneractive.parameters.category = "Lifestyle";
				
				if (storage.geolocation) {
					inneractive.parameters.GPSLocation = storage.geolocation.latitude + ',' + storage.geolocation.longitude;
				}
				
				inneractive.parameters.divID = adsContainerId;
				inneractive.parameters.appID = naxAppId;
				
				inneractive.displayInneractiveAd();
			}
		}
	}
	
	function _ajaxWrapper(requestURL, isAsync) {
		var request = new XMLHttpRequest();
		request.open('GET', requestURL, isAsync === true);
		request.send();
		
		return {
			status: request.status,
			body: request.responseText
		};
	};
	
	function _log(obj) {
		if (DEBUG) {
			console.log(obj);
		}
	};
	
	function _eval(body) {
		return eval('(' + body + ')');
	};
	
	function _removePageContent() {
		// Remove other page content
		var detailContainer = document.getElementById('flight-detail-list');
		if (detailContainer) {
			detailContainer.innerHTML = '';
		}
		
		var mainContainer = document.getElementById('flight-result-list');
		if (mainContainer) {
			mainContainer.innerHTML = '';
		}
		
		var departureMapContainer = document.getElementById('airport-map-departure');
		if (departureMapContainer) {
			departureMapContainer.innerHTML = '';
		}
		
		var arrivalMapContainer = document.getElementById('airport-map-arrival');
		if (arrivalMapContainer) {
			arrivalMapContainer.innerHTML = '';
		}
		
		// Remove error message
		var errorContainer = document.getElementById('error-container');
		if (errorContainer) {
			errorContainer.innerHTML = '';
		}
	};
	
	function _backToSearchForm() {
		_setMainAction(null);
		
		_removePageContent();
		
		mwl.switchClass('#progress-anim', 'progress-show', 'progress-hide');
		mwl.setGroupTarget('#main-content-wrapper', '#all-content-container', 'show', 'hide');
		mwl.setGroupTarget('#all-content-container', '#main-search-form', 'show', 'hide');
		
		_setHeaderBreadcrumb(null);
	};
	
	function _initiateSearchForm() {
		_backToSearchForm();
		
		_log(AIRPORT_DATA);
		
		// Just take static airport data :)
		var airportText = '<form>';
		
		var selectDeparture = '<p class="label">' + string_departure_airport + '</p><select class="airport-combo-box" id="departure-airport"><option selected="" value="">' + string_departure_hint + '</option>';
		var selectArrival = '<p class="label">' + string_arrival_airport + '</p><select class="airport-combo-box" id="arrival-airport"><option selected="" value="">' + string_arrival_hint + '</option>';
		
		var dataLength = AIRPORT_DATA.all_airport.airport.length;
		for (var i = 0; i < dataLength; i++) {
			airport = AIRPORT_DATA.all_airport.airport[i];
			
			if (airport != null) {
				code = airport.airport_code;
				name = airport.airport_name;
				locationName = airport.location_name;
				
				tempString = '<option value="' + code + '">' + locationName + '(' + code + ')</option>';
				
				selectArrival += tempString;
				selectDeparture += tempString;
			}
		}
		
		selectDeparture += '</select><br/>';
		selectArrival += '</select>';
		
		airportText += selectDeparture + selectArrival + '</form>';
		
		document.getElementById('airport_option').innerHTML = airportText;
		
		// populate date String
		var departDate = new Date();
		departDate.setDate(departDate.getDate() + 1);
		
		var returnDate = new Date();
		returnDate.setDate(returnDate.getDate() + 2);
		
		var cmbDepartDay = document.getElementById('depart-date-day');
		var cmbReturnDay = document.getElementById('return-date-day');
		for (var i = 1; i <= 31; i++) {
			var dayElem = document.createElement('option');
			dayElem.setAttribute('value', i);
			dayElem.innerHTML = i;
			
			var dayElem2 = document.createElement('option');
			dayElem2.setAttribute('value', i);
			dayElem2.innerHTML = i;
			
			cmbDepartDay.appendChild(dayElem);
			cmbReturnDay.appendChild(dayElem2);
			
			if (departDate.getDate() == i) {
				cmbDepartDay.value = i;
			}
			
			if (returnDate.getDate() == i) {
				cmbReturnDay.value = i;
			}
		}
		
		var cmbDepartMonth = document.getElementById('depart-date-month');
		var cmbReturnMonth = document.getElementById('return-date-month');
		for (var i = 1; i <= 12; i++) {
			var monthElem = document.createElement('option');
			monthElem.setAttribute('value', i);
			monthElem.innerHTML = i;
			
			var monthElem2 = document.createElement('option');
			monthElem2.setAttribute('value', i);
			monthElem2.innerHTML = i;
			
			cmbDepartMonth.appendChild(monthElem);
			cmbReturnMonth.appendChild(monthElem2);
			
			if (returnDate.getMonth() + 1 == i) {
				cmbDepartMonth.value = i;
			}
			
			if (departDate.getMonth() + 1 == i) {
				cmbReturnMonth.value = i;
			}
		}
		
		var cmbDepartYear = document.getElementById('depart-date-year');
		var cmbReturnYear = document.getElementById('return-date-year');
		
		cmbDepartYear.innerHTML = '';
		cmbReturnYear.innerHTML = '';
		
		var fullYear = departDate.getFullYear();
		for (var i = fullYear; i <= fullYear + 1; i++) {
			var yearElem = document.createElement('option');
			yearElem.setAttribute('value', i);
			yearElem.innerHTML = i;
			
			var yearElem2 = document.createElement('option');
			yearElem2.setAttribute('value', i);
			yearElem2.innerHTML = i;
			
			cmbDepartYear.appendChild(yearElem);
			cmbReturnYear.appendChild(yearElem2);
			
			if (returnDate.getFullYear() == i) {
				cmbDepartYear.value = i;
			}
			
			if (departDate.getFullYear() == i) {
				cmbReturnYear.value = i;
			}
		}
	};
	
	function _getFlightSearch() {
		var url = tiketSearchUrl + 'token=' + storedToken + '&d=SUB&a=CGK&date=2013-03-10&adult=1';
		var response = _ajaxWrapper(url);
		
		if (response.status != 200) {
			
		} else {
			jsonResponse = _eval(response.body);
			_log(jsonResponse);
			
			if (jsonResponse.diagnostic.status == 200) {
			}
		}
	};
	
	function _getToken() {
		var url = tiketTokenUrl;
		var response = _ajaxWrapper(url);
		
		if (response.status != 200) {
			
		} else {
			jsonResponse = _eval(response.body);
			_log(jsonResponse);
			
			if (jsonResponse.diagnostic.status == 200) {
				storedToken = jsonResponse.token;
				
				document.getElementById("token-container").value = storedToken;
			}
		}
	};
	
	function _getPromo() {
		var url = tiketPromoUrl + 'token=' + storedToken;
		var response = _ajaxWrapper(url);
		
		if (response.status != 200) {
			
		} else {
			jsonResponse = _eval(response.body);
			
			_log(jsonResponse);
			
			var promoElem = document.getElementById("promo-container");
			promoElem.innerHTML = '';
			
			var dataLength = jsonResponse.promos.result.length;
			var formattedData = Array();
			for (var i = 0; i < dataLength; i++) {
				var promo = jsonResponse.promos.result[i];
				
				var image = promo.image;
				var flightDateString = promo.flight_date;
				var flightDate = _parseDate(flightDateString);
				
				var airlineName = promo.airlines_name;
				var flightNumber = promo.flight_number;
				
				var departureName = promo.departure_city_name;
				var departureCode = promo.departure_city;
				var arrivalName = promo.arrival_city_name;
				var arrivalCode = promo.arrival_city;
				
				var departTime = promo.simple_departure_time;
				var arriveTime = promo.simple_arrival_time;
				var duration = promo.duration.replace('j', string_hour_short);
				
				var price = parseInt(promo.price_value, 10);
				var itemAction = 
					TRANSITION_FUNCTION + 
					'Travelas.searchFlightByPromo(\'' + departureCode + '\', \'' + arrivalCode + '\', \'' + flightDateString + '\')';
				
				formattedData.push({
					image: image,
					action: itemAction,
					line1: departureName + ' ' + string_to + ' ' + arrivalName,
					line2: flightDate.format('dddd, dd mmm yyyy'),
					line3: departTime + ' - ' + arriveTime + ' (' + duration + ')',
					line4: '<strong style=\'font-size: medium;\'>' + 'IDR ' + numeral(price).format('0,0') + '</strong>'
				});
			}
			
			_generateClickableFlightList(promoElem, formattedData);
		}
	};
	
	function _generateClickableFlightList(parentContainer, formattedData) {
		var dataLength = formattedData.length;
		for (var i = 0; i < dataLength; i++) {
			var data = formattedData[i];
			
			var listWrapper = document.createElement('a');
			if (data.action) {
				listWrapper.setAttribute('onclick', data.action);
			}
			
			var listEntry = document.createElement('table');
			listEntry.setAttribute('cellpadding', '0');
			listEntry.setAttribute('cellspacing', '0');
			listWrapper.appendChild(listEntry);
			
			var theClass = 'list-entry';
			if (data.isSelected) {
				theClass += ' list-selected';
			}
			
			if (i == dataLength - 1) {
				theClass += ' list-entry-bottom';
			}
			
			listEntry.setAttribute('class', theClass);
			parentContainer.appendChild(listWrapper);
			
			// First Row
			var row = document.createElement('tr');
			listEntry.appendChild(row);
			
			if (data.image) {
				var col1 = document.createElement('td');
				col1.setAttribute('rowspan', '4');
				col1.setAttribute('class', 'list-icon');
				col1.innerHTML = '<img src="' + data.image + '" alt="icon" />';
				row.appendChild(col1);
			}
			
			var col2 = document.createElement('td');
			col2.innerHTML = '<p>' + data.line1 + '</p>';
			row.appendChild(col2);
			
			// Second Row
			if (data.line2) {
				row = document.createElement('tr');
				listEntry.appendChild(row);
				
				col2 = document.createElement('td');
				col2.innerHTML = '<p>' + data.line2 + '</p>';
				row.appendChild(col2);
			}
			
			if (data.line3) {
				// Third Row
				row = document.createElement('tr');
				listEntry.appendChild(row);
				
				col2 = document.createElement('td');
				col2.innerHTML = '<p>' + data.line3 + '</p>';
				row.appendChild(col2);
			}
			
			if (data.line4) {
				// Third Row
				row = document.createElement('tr');
				listEntry.appendChild(row);
				
				col2 = document.createElement('td');
				col2.innerHTML = '<p>' + data.line4 + '</p>';
				row.appendChild(col2);
			}
		}
		
		var btnBackToTop = document.createElement('div');
		parentContainer.appendChild(btnBackToTop);

		btnBackToTop.setAttribute('class', 'list-to-top');
		btnBackToTop.setAttribute('onclick', "mwl.scrollTo('#ui-header');");
		btnBackToTop.innerHTML = '<p>' + string_back_to_top + '</p>';
	};
	
	function _searchFlightByPromo(departureCode, arrivalCode, flightDateString) {
		var response = _flightQuery(departureCode, arrivalCode, flightDateString, null, 1, 0, 0);
		if (response != false) {
			var departDateObject = _parseDate(flightDateString);
			flightSearchResult = response;
			
			_displayDepartFlightResult(flightSearchResult, departDateObject);
		}
	}
	
	function _searchFlight() {
		mwl.show('#all-content-container');
		
		mwl.switchClass('#progress-anim', 'progress-show', 'progress-hide');
		mwl.show('#btn-search-flight');
	
		var errorContainer = document.getElementById('error-container');
		errorContainer.innerHTML = '';
		
		// Getting airport choice
		var departureAirport = document.getElementById('departure-airport');
		var arrivalAirport = document.getElementById('arrival-airport');
		
		var departureChoice = departureAirport.options[departureAirport.selectedIndex].value;
		var arrivalChoice = arrivalAirport.options[arrivalAirport.selectedIndex].value;
		
		if (departureChoice == '' || arrivalChoice == '') {
			errorContainer.innerHTML += '<p>' + string_error_airports_no_choice + '</p>';
		} else if (departureChoice == arrivalChoice) {
			errorContainer.innerHTML += '<p>' + string_error_airports_same + '</p>';
		}
		
		// Getting date choice
		var departDayElem = document.getElementById('depart-date-day');
		var departMonthElem = document.getElementById('depart-date-month');
		var departYearElem = document.getElementById('depart-date-year');
		
		var currentDateObject = new Date();
		var chkRoundTrip = document.getElementById('is-roundtrip');
		
		var departDateObject = _verifyDate(departDayElem, departMonthElem, departYearElem, currentDateObject);
		var returnDateObject = null;
		if (departDateObject == false) {
			errorContainer.innerHTML += '<p>' + string_error_invalid_departure_date + currentDateObject.format('dd mmm yyyy') + '.</p>';
		} else if (chkRoundTrip.checked) {
			// check return date
			var returnDayElem = document.getElementById('return-date-day');
			var returnMonthElem = document.getElementById('return-date-month');
			var returnYearElem = document.getElementById('return-date-year');
			
			returnDateObject = _verifyDate(returnDayElem, returnMonthElem, returnYearElem, departDateObject);
			
			if (!returnDateObject) {
				errorContainer.innerHTML += '<p>' + string_error_invalid_return_date + departDateObject.format('dd mmm yyyy') + '.</p>';	
			}
		}
		
		var adultCount = document.getElementById('psg-adult').options[document.getElementById('psg-adult').selectedIndex].value;
		var childCount = document.getElementById('psg-child').options[document.getElementById('psg-child').selectedIndex].value;
		var infantCount = document.getElementById('psg-infant').options[document.getElementById('psg-infant').selectedIndex].value;
		if (infantCount > adultCount) {
			errorContainer.innerHTML += '<p>' + string_error_too_many_infant + '</p>';
		}
		
		if (errorContainer.innerHTML == '') {
			// TODO: must save departure, arrival, passenger counts to storage.
			_savePreference('departure', departureChoice);
			_savePreference('arrival', arrivalChoice);
			_savePreference('adult', adultCount);
			_savePreference('child', childCount);
			_savePreference('infant', infantCount);
			
			var departDateString = departDateObject.format('yyyy-mm-dd');
			var returnDateString = null;
			if (returnDateObject != null && returnDateObject != false) {
				returnDateString = returnDateObject.format('yyyy-mm-dd');
			}
			
			var response = _flightQuery(departureChoice, arrivalChoice, departDateString, returnDateString, adultCount, childCount, infantCount);
			if (response == false) {
			} else {
				flightSearchResult = response;
				_displayDepartFlightResult(flightSearchResult, departDateObject);
			}
		}	
	}
	
	function _flightQuery(departureChoice, arrivalChoice, departDateString, returnDateString, adultCount, childCount, infantCount) {
		var urlParamString = tiketSearchUrl 
			+ '&token=' + storedToken
			+ '&d=' + departureChoice 
			+ '&a=' + arrivalChoice 
			+ '&date=' + departDateString
			+ '&adult=' + adultCount
			+ '&child=' + childCount
			+ '&infant=' + infantCount
			+ '&sort=' + defaultSort
			+ '&v=3';
		
		var dateNow = new Date();
		var urlUpdateString = tiketCheckUpdateUrl
			+ '&token=' + storedToken
			+ '&d=' + departureChoice 
			+ '&a=' + arrivalChoice 
			+ '&date=' + departDateString
			+ '&adult=' + adultCount
			+ '&child=' + childCount
			+ '&infant=' + infantCount
			+ '&time=' + dateNow.getTime()
			+ '&v=3';
		
		if (returnDateString != null && returnDateString != false) {
			urlParamString += '&ret_date=' + returnDateString;
			urlUpdateString += '&ret_date=' + returnDateString;
		}
		
		_log(urlParamString);
		
		var response = _ajaxWrapper(urlParamString);
		
		// HACK request for checkupdate first
		_ajaxWrapper(urlUpdateString);
//		response = _ajaxWrapper(urlParamString);
		
		if (response.status != 200) {	
			var errorContainer = document.getElementById('error-container');
			errorContainer.innerHTML = '';
			errorContainer.innerHTML += '<p>' + SERVER_ERROR_STRING + '</p>';
			
			return false;
		} else {
			// Remove display element and cache
			_removePageContent();
			flightSearchResult = null;
			
			jsonResponse = _eval(response.body);
			
			_log(jsonResponse);
			
			if (jsonResponse.diagnostic.status == 200) {
				
				// process nearby return date glitch here...
				if (jsonResponse.round_trip) {
					var departDate = _parseDate(jsonResponse.search_queries.date);
					departDate.setHours(0, 0, 0, 0);
					var nearbyDateCount = jsonResponse.nearby_ret_date.nearby.length;
					
					for (var i = 0; i < nearbyDateCount; i++) {
						var datePrice = jsonResponse.nearby_ret_date.nearby[i];
						var datePriceDate = _parseDate(datePrice.date);
						datePriceDate.setHours(0, 0, 0, 0);
						
						if (datePriceDate.getTime() < departDate.getTime()) {
							// should remove nearby return date which is less than depart date
							jsonResponse.nearby_ret_date.nearby.splice(i, 1);
							nearbyDateCount--;
							i--;
						}
					}
				}
				
				return jsonResponse;
			} else {
				return false;
			}
		}
	}
	
	function _getNewDepartFlight(dateString) {
		var savedQuery = flightSearchResult.search_queries;
		var newDate = _parseDate(dateString);
		savedQuery.date = dateString;
		
		// Check return date, add 1 day if return is less than depart date
		if (savedQuery.ret_date != false) {
			newDate.setHours(0, 0, 0, 0);
			var retDate = _parseDate(savedQuery.ret_date);
			retDate.setHours(0, 0, 0, 0);
			
			if (retDate.getTime() < newDate.getTime()) {
				retDate.setDate(retDate.getDate() + 1);
				savedQuery.ret_date = retDate.format('yyyy-mm-dd');
			}
		}
		
		var response = _flightQuery(savedQuery.from, savedQuery.to, dateString, savedQuery.ret_date, savedQuery.adult, savedQuery.child, savedQuery.infant);
		if (response == false) {
		} else {
			flightSearchResult = response;
			_displayDepartFlightResult(flightSearchResult, newDate);
			
			// set the timer
		}
	}
	
	function _getNewReturnFlight(dateString) {
		var savedQuery = flightSearchResult.search_queries;
		var newReturnDate = _parseDate(dateString);
		newReturnDate.setHours(0, 0, 0, 0);
		
		var oldReturnDate = _parseDate(savedQuery.ret_date);
		oldReturnDate.setHours(0, 0, 0, 0);
		
		var departDate = _parseDate(savedQuery.date);
		departDate.setHours(0, 0, 0, 0);
		
		// Check whether new return date is less than depart date
		if (newReturnDate.getTime() < departDate.getTime()) {
			// TODO: show error message
			
			// Show previous result, unchanged
			_displayReturnFlightResult(flightSearchResult, oldReturnDate);
		} else {
			var response = _flightQuery(savedQuery.from, savedQuery.to, savedQuery.date, dateString, savedQuery.adult, savedQuery.child, savedQuery.infant);
			if (response == false) {
				
			} else {
				flightSearchResult = response;
				_displayReturnFlightResult(flightSearchResult, newReturnDate);
			}
		}
	}
	
	function _displayDepartFlightResult(flightSearchResult, dateObject) {
		_setMainAction('Travelas.backToSearchForm();');
		
		storage.departDateObject = dateObject;
		_displayFlightResult(flightSearchResult, dateObject, 'go_det', 'departures', 'nearby_go_date', 'getNewDepartFlight', string_title_departure_flight);
	
//		mwl.timer('departRefresh', 20000, 20, "_log('REFRESH AUTO');");
//		mwl.timer('departRefresh', 10000, 10, "mwl.stopTimer(); Travelas.getNewDepartFlight('" + dateObject.format('yyyy-mm-dd') + "')");
	}
	
	function _displayReturnFlightResult(flightSearchResult, dateObject) {
		var departDateString = flightSearchResult.search_queries.date;
		_setMainAction('Travelas.getNewDepartFlight(\'' + departDateString + '\');');
		
		storage.returnDateObject = dateObject;
		_displayFlightResult(flightSearchResult, dateObject, 'ret_det', 'returns', 'nearby_ret_date', 'getNewReturnFlight', string_title_return_flight);
	
//		mwl.timer('returnRefresh', 10000, 10, "mwl.stopTimer(); Travelas.getNewReturnFlight('" + dateObject.format('yyyy-mm-dd') + "')");
	}
	
	function _setMainAction(myFunctionName) {
		// set main action
		var btnMainAction = document.getElementById('btn-main-action');
		
		if (myFunctionName == null) {
			mwl.addClass('#btn-main-action', 'hide');
			mwl.removeClass('#btn-main-action', 'inline');
		} else {
			mwl.removeClass('#btn-main-action', 'hide');
			mwl.addClass('#btn-main-action', 'inline');
			btnMainAction.setAttribute('onclick', TRANSITION_FUNCTION + myFunctionName);	
		}
	}
	
	function _setHeaderBreadcrumb(breadcrumbText) {
		var header = document.getElementById('header-breadcrumb');
		header.innerHTML = '';
		
		if (breadcrumbText != null) {
			mwl.show('#header-breadcrumb');
			header.innerHTML = '<p>' + breadcrumbText + '</p>';
		} else {
			mwl.hide('#header-breadcrumb');
		}
	}
	
	/**
	 * choose depart flight, check whether flightResultData is rountrip / not
	 */
	function _chooseDepartFlight(selectedDepartFlight) {
		var departFlightContainer = document.getElementById('selected-depart-flight-container');
		departFlightContainer.value = selectedDepartFlight;
		
		storage.departFlightObject = _eval(selectedDepartFlight);
		
		// remove selected return flight value
		document.getElementById('selected-return-flight-container').value = '';
		
		if (flightSearchResult.round_trip) {
			// show flight return data
			var retDateString = flightSearchResult.search_queries.ret_date;
			_displayReturnFlightResult(flightSearchResult, _parseDate(retDateString));
		} else {
			// proceed to flights detail
			_flightsDetail();
		}
	}
	
	/**
	 * choose return flight
	 */
	function _chooseReturnFlight(selectedReturnFlight) {
		var returnFlightContainer = document.getElementById('selected-return-flight-container');
		returnFlightContainer.value = selectedReturnFlight;
		
		storage.returnFlightObject = _eval(selectedReturnFlight);
		
		// TODO: Open flight detail view
		_flightsDetail();
	}
	
	function _flightsDetail() {
		var isRoundTrip = flightSearchResult.round_trip;
		var myAction = null;
		if (isRoundTrip) {
			var returnDateString = flightSearchResult.search_queries.ret_date;
			myAction = 'Travelas.getNewReturnFlight(\'' + returnDateString + '\');';
		} else {
			var departDateString = flightSearchResult.search_queries.date;
			myAction = 'Travelas.getNewDepartFlight(\'' + departDateString + '\');';
		}
		
		_setMainAction(myAction);
		_setHeaderBreadcrumb(string_title_flight_detail);
		
		mwl.switchClass('#progress-anim', 'progress-show', 'progress-hide');
		mwl.setGroupTarget('#main-content-wrapper', '#all-content-container', 'show', 'hide');
		mwl.setGroupTarget('#all-content-container', '#flight-detail-display', 'show', 'hide');
		
		var departFlightContainerValue = document.getElementById('selected-depart-flight-container').value;
		var returnFlightContainerValue = document.getElementById('selected-return-flight-container').value;
		
		var departFlight = storage.departFlightObject;
		
		_removePageContent();
		var detailContainer = document.getElementById('flight-detail-list');
		
		var totalPrice = _renderFlightDetail(detailContainer, departFlight, 'go_det', 'date', string_departure_flight);
		if (isRoundTrip) {
			var returnFlight = storage.returnFlightObject;
			totalPrice += _renderFlightDetail(detailContainer, returnFlight, 'ret_det', 'ret_date', string_return_flight);
		}
		
		// render total price 
		var totalContainer = document.createElement('div');
		totalContainer.setAttribute('class', 'flight-detail-total');
		detailContainer.appendChild(totalContainer);
		
		var detailPriceTable = document.createElement('table');
		detailPriceTable.setAttribute('class', 'fill-detail');
		detailPriceTable.setAttribute('cellspacing', 0);
		detailPriceTable.setAttribute('cellpadding', 0);
		totalContainer.appendChild(detailPriceTable);
		
		var row1 = document.createElement('tr');
		detailPriceTable.appendChild(row1);
		
		var col1 = document.createElement('td');
		row1.appendChild(col1);
		col1.setAttribute('class', 'text-left');
		col1.innerHTML = '<h3>TOTAL</h3>';
		
		var col2 = document.createElement('td');
		row1.appendChild(col2);
		col2.setAttribute('class', 'text-right');
		col2.innerHTML = '<h3>IDR ' + numeral(totalPrice).format('0,0') + '</h3>';
		
		var disclaimer = document.createElement('p');
		totalContainer.appendChild(disclaimer);
		disclaimer.setAttribute('style', "padding-top: 10px;");
		disclaimer.innerHTML = '* ' + string_exclude_tax;
		
		// Show ads on detail page
		// _showAds();
		
		// Prepare booking URL
		_bookToPartner();
	}
	
	function _renderFlightDetail(detailContainer, choosenFlightData, destinationKey, dateKey, detailTitle) {
		
		var departureAirport = flightSearchResult[destinationKey].dep_airport;
		var arrivalAirport = flightSearchResult[destinationKey].arr_airport;
		var departureDate = _parseDate(flightSearchResult.search_queries[dateKey]);
		
		var adult = flightSearchResult.search_queries.adult;
		var child = flightSearchResult.search_queries.child;
		var infant = flightSearchResult.search_queries.infant;
		var duration = choosenFlightData.duration.replace('j', 'h');
		
		var totalAdult = adult * choosenFlightData.price_adult;
		var totalChild = child * choosenFlightData.price_child;
		var totalInfant = infant * choosenFlightData.price_infant;
		var totalPrice = totalAdult + totalChild + totalInfant;
		
		var detailItem = document.createElement('div');
		detailItem.setAttribute('class', 'flight-detail-item');
		detailContainer.appendChild(detailItem);
		
		var detailHeader = document.createElement('div');
		detailHeader.setAttribute('class', 'flight-detail-header');
		detailItem.appendChild(detailHeader);
		detailHeader.innerHTML = 
			'<table cellspacing="0" cellpadding="0" style="width: 100%;">' +
				'<tr><td>' + '<p><strong>' + detailTitle + ' (' + departureAirport.airport_code + ' - ' + arrivalAirport.airport_code + ')</strong></p>' + '</td>' + 
					'<td rowspan="2" style="width: 32px;">' + '<!-- <div class="hide button-square-small sms-icon">&nbsp;</div> -->' + '</td></tr>' + 
				'<tr><td><p>' + departureDate.format('dddd, dd mmm yyyy') + '</p></td></tr>' +
			'</table>';
		
		var detailPrice = document.createElement('div');
		detailPrice.setAttribute('class', 'flight-detail-price');
		detailItem.appendChild(detailPrice);
		
		var detailPriceTable = document.createElement('table');
		detailPriceTable.setAttribute('class', 'fill-detail');
		detailPriceTable.setAttribute('cellspacing', 0);
		detailPriceTable.setAttribute('cellpadding', 0);
		detailPrice.appendChild(detailPriceTable);
		
		var row1 = document.createElement('tr');
		detailPriceTable.appendChild(row1);
		
		var col1 = document.createElement('td');
		row1.appendChild(col1);
		col1.setAttribute('rowspan', 2);
		col1.setAttribute('style', "width: 39px; padding-right: 5px;");
		col1.innerHTML = '<img src="' + choosenFlightData.image + '" />';
		
		var col2 = document.createElement('td');
		row1.appendChild(col2);
		col2.innerHTML = '<p>' + choosenFlightData.airlines_name + '</p>';
		
		var col3 = document.createElement('td');
		row1.appendChild(col3);
		col3.setAttribute('rowspan', 2);
		col3.setAttribute('class', 'text-right');
		col3.innerHTML = '<p>IDR ' + numeral(totalPrice).format('0,0') + '</p>';
		
		var row2 = document.createElement('tr');
		detailPriceTable.appendChild(row2);
		
		col2 = document.createElement('td');
		row2.appendChild(col2);
		col2.innerHTML = '<p>' + choosenFlightData.flight_number + '</p>';
	
		var summary = document.createElement('div');
		summary.setAttribute('class', 'flight-detail-summary clear');
		detailItem.appendChild(summary);
		summary.innerHTML = 
			'<p>' + departureAirport.location_name + ' (' + departureAirport.airport_code + ') - ' + arrivalAirport.location_name + ' (' + arrivalAirport.airport_code + ')</p>' +
			'<p>' + choosenFlightData.simple_departure_time + ' - ' + choosenFlightData.simple_arrival_time + ' (' + duration + ')</p>' + 
			'<p>' + adult + ' ' + string_adults + ' x IDR ' + numeral(choosenFlightData.price_adult).format('0,0') + '</p>';
	
		if (child > 0) {
			summary.innerHTML += '<p>' + child + ' ' + string_childs + ' x IDR ' + numeral(choosenFlightData.price_child).format('0,0') + '</p>';
		}
		
		if (infant > 0) {
			summary.innerHTML += '<p>' + infant + ' ' + string_infants + ' x IDR ' + numeral(choosenFlightData.price_infant).format('0,0') + '</p>';
		}
		
		var detailFooter = document.createElement('div');
		
		// TODO: uncomment this on next version, commented because waiting for exact store URL
		detailItem.appendChild(detailFooter);
		detailFooter.setAttribute('class', 'flight-detail-footer');
		
		var rawMessage = string_share_message;
		rawMessage = rawMessage.replace('{airline}', choosenFlightData.airlines_name + '(' + choosenFlightData.flight_number + ')');
		rawMessage = rawMessage.replace('{from}', departureAirport.location_name);
		rawMessage = rawMessage.replace('{to}', arrivalAirport.location_name);
		rawMessage = rawMessage.replace('{date}', departureDate.format('dd mmm yyyy'));
		rawMessage = rawMessage.replace('{price}', numeral(choosenFlightData.price_adult).format('0,0'));
		rawMessage = rawMessage.replace('{appurl}', shortAppUrl);
		
		_generateShareButtons(detailFooter, rawMessage);
		
		return totalPrice;
	}
	
	function _generateShareButtons(parentContainer, messageContent) {
		_log(messageContent);
		
		var encodedMessage = encodeURI(messageContent);
		
		var smsUri = 'sms:?body=' + encodedMessage;
		var facebookShareUri = 'https://m.facebook.com/dialog/send?app_id=123050457758183&name=People%20Argue%20Just%20to%20Win&link=http://www.nytimes.com/2011/06/15/arts/people-argue-just-to-win-scholars-assert.html&redirect_uri=https://www.bancsabadell.com/cs/Satellite/SabAtl/';
		var twitterShareUri = 'https://mobile.twitter.com/compose/tweet?status=' + encodedMessage;
		
		var shareTable = document.createElement('table');
		parentContainer.appendChild(shareTable);
		shareTable.setAttribute('cellspacing', 0);
		shareTable.setAttribute('cellpadding', 0);
		
		row1 = document.createElement('tr');
		shareTable.appendChild(row1);
		
		col1 = document.createElement('td');
		row1.appendChild(col1);
		col1.setAttribute('class', 'first-share-column');
		col1.innerHTML = '<p><strong>' + string_share_to_friends + ':</strong></p>';
		
		// Facebook share is not viable, it doesn't permit us to post specific content on "what's on your mind" field.
//		col2 = document.createElement('td');
//		row1.appendChild(col2);
//		col2.innerHTML = '<a href="' + facebookShareUri + '"><div class="button-square-small fb-icon">&nbsp;</div></a>';
		
		col3 = document.createElement('td');
		row1.appendChild(col3);
		col3.innerHTML = '<a href="' + twitterShareUri + '"><div class="button-square-small twitter-icon">&nbsp;</div></a>';
		
		var col4 = document.createElement('td');
		row1.appendChild(col4);
		col4.innerHTML = '<a href="' + smsUri + '"><div class="button-square-small sms-icon">&nbsp;</div></a>';
	}
	
	function _bookToPartner() {
		if (storage.departFlightObject != null) {
			var redirUrl = tiketRedirOrderUrl;
			var departFlight = storage.departFlightObject;
			var departFlightId = departFlight.flight_id;
			
			redirUrl += '&flight=' + departFlightId;
			
			if (flightSearchResult.round_trip && storage.returnFlightObject != null) {
				var returnFlight = storage.returnFlightObject;
				var returnFlightId = returnFlight.flight_id;
				
				redirUrl += '&ret_flight=' + returnFlightId;
			}
			
			_log(redirUrl);
			
			// OVERRIDE BOOKING TO USE PHONE, 
			// IT IS CURRENTLY IMPOSSIBLE TO USE WEBSITE REDIRECTION
			redirUrl = 'tel:+622183782020';
			
			// Set the #btn-book-flight onclick action
			var button = document.getElementById('btn-book-flight');
			button.setAttribute('onclick', "mwl.loadURL('" + encodeURI(redirUrl) + "')");
			
			_showInlineAds(naxInlineContainerId);
		}
	}
	
	function _tentativeNumeral(price) {
		if (price > 0) {
			return numeral(price).format('0,0');
		} else {
			return '?';
		}
	}
	
	/**
	 * TODO: flight result ini nanti ada 2, depart flight & return flight, bagaimana cara akomodasinya biar
	 * fungsi ini bisa reusable?
	 */
	function _displayFlightResult(flightSearchResult, dateObject, detailKey, listKey, nearbyKey, actionFunctionName, headerBreadcrumb) {
		mwl.show('#all-content-container');
		
		mwl.switchClass('#progress-anim', 'progress-show', 'progress-hide');
		mwl.setGroupTarget('#main-content-wrapper', '#all-content-container', 'show', 'hide');
		mwl.setGroupTarget('#all-content-container', '#flight-result-display', 'show', 'hide');
		mwl.setGroupTarget('#flight-result-display', '#flight-result-list', 'show', 'hide');
		
		_removePageContent();
		var mainContainer = document.getElementById('flight-result-list');
		
		_setHeaderBreadcrumb(headerBreadcrumb);
		
		var departDateShort = dateObject.format('yyyy-mm-dd');
		var nearbyDateCount = flightSearchResult[nearbyKey].nearby.length;
		var dateIndex = -1;
		var formattedNearbyDate = Array();
		
		var departDateObject = null;
		if (listKey == 'returns') {
			// if searching return flights, consider depart flight date
			departDateObject = _parseDate(flightSearchResult.search_queries.date);
			departDateObject.setHours(0, 0, 0, 0);
		}
		
		for (var i = 0; i < nearbyDateCount; i++) {
			var datePrice = flightSearchResult[nearbyKey].nearby[i];
			if (datePrice.date == departDateShort) {
				dateIndex = i;
			}
			
			var dateObject = _parseDate(datePrice.date);
			var isSelected = (i == dateIndex);
			var action = TRANSITION_FUNCTION + 'Travelas.' + actionFunctionName + '(\'' + datePrice.date + '\')';
			if (isSelected) {
				action = "mwl.removeClass('#btn-main-action', 'hide'); mwl.addClass('#btn-main-action', 'inline'); mwl.setGroupTarget('#flight-result-display', '#flight-result-list', 'show', 'hide')";
			}
			
			formattedNearbyDate.push({
				action: action,
				isSelected: isSelected,
				line1: dateObject.format('dddd, dd mmm yyyy'),
				line2: string_starts + ' ' + string_from + ' <strong>IDR ' + _tentativeNumeral(datePrice.price) + '</strong>'
			});
		}
		
		var navigationContainer = document.createElement('div');
		navigationContainer.setAttribute('class', 'navigation-container');
		mainContainer.appendChild(navigationContainer);
		
		var prevNav = document.createElement('div');
		prevNav.setAttribute('class', 'nav-item nav-item-left');
		navigationContainer.appendChild(prevNav);
		
		var nextNav = document.createElement('div');
		nextNav.setAttribute('class', 'nav-item nav-item-right');
		navigationContainer.appendChild(nextNav);
		
		var action = TRANSITION_FUNCTION + "Travelas." + actionFunctionName + '(\'';
		
		if (dateIndex > 0) {
			// add navigation to previous date
			var datePrice = flightSearchResult[nearbyKey].nearby[dateIndex - 1];
			var prevDate = _parseDate(datePrice.date);
			prevDate.setHours(0, 0, 0, 0);
			
			prevNav.setAttribute('onclick', action + prevDate.format('yyyy-mm-dd') + '\')');
			prevNav.innerHTML = '<p class="top">' + prevDate.format('ddd, dd mmm yyyy') + '</p><p class="bottom">IDR ' + _tentativeNumeral(datePrice.price) + '</p>';
		} else {
			nextNav.setAttribute('class', 'nav-item nav-item-right nav-item-right-bordered');
		}
		
		if (dateIndex < nearbyDateCount - 1) {
			// add navigation to next date
			var datePrice = flightSearchResult[nearbyKey].nearby[dateIndex + 1];
			var nextDate = _parseDate(datePrice.date);
			nextNav.setAttribute('onclick', action + nextDate.format('yyyy-mm-dd') + '\')');
			nextNav.innerHTML = '<p class="top">' + nextDate.format('ddd, dd mmm yyyy') + '</p><p class="bottom">IDR ' + _tentativeNumeral(datePrice.price) + '</p>';
		}
		
		// Current breadcrumbs
		var breadcrumb = document.createElement('table');
		breadcrumb.setAttribute('class', 'list-entry');
		breadcrumb.setAttribute('cellpadding', '0');
		breadcrumb.setAttribute('cellspacing', '0');
		breadcrumb.setAttribute('style', 'padding: 5px;');
		mainContainer.appendChild(breadcrumb);
		
		var currentDatePrice = flightSearchResult[nearbyKey].nearby[dateIndex];
		var departureAirport = flightSearchResult[detailKey].dep_airport;
		var arrivalAirport = flightSearchResult[detailKey].arr_airport;
		
		var row1 = document.createElement('tr');
		breadcrumb.appendChild(row1);
		
		var col1 = document.createElement('td');
		col1.setAttribute('colspan', '4');
		col1.innerHTML = '<p>' + departureAirport.location_name + ' (' + departureAirport.airport_code + ')' + ' ' + string_to + ' ' + arrivalAirport.location_name + ' (' + arrivalAirport.airport_code + ')' + '</p>';
		row1.appendChild(col1);
		
		var row2 = document.createElement('tr');
		breadcrumb.appendChild(row2);
		
		var dateObject = _parseDate(currentDatePrice.date);
		col1 = document.createElement('td');
		col1.innerHTML = '<p><strong>' + dateObject.format('ddd, dd mmm yyyy') + '</strong></p>';
		row2.appendChild(col1);
		
		var col2 = document.createElement('td');
		col2.setAttribute('rowspan', 2);
		row2.appendChild(col2);
		
		var col3 = document.createElement('td');
		col3.setAttribute('rowspan', 2);
		row2.appendChild(col3);
		
		var col4 = document.createElement('td');
		col4.setAttribute('rowspan', 2);
		row2.appendChild(col4);
		
		var refreshFunction = 'Travelas.' + actionFunctionName + '(\'' + currentDatePrice.date + '\')';
		var refreshButton = document.createElement('div');
		refreshButton.setAttribute('class', 'button-square-small myrefresh-icon');
		refreshButton.setAttribute('onclick', TRANSITION_FUNCTION + refreshFunction);
		refreshButton.innerHTML = '&nbsp;';
		col2.appendChild(refreshButton);
		
		var listButton = document.createElement('div');
		listButton.setAttribute('class', 'button-square-small calendar-icon');
		listButton.setAttribute('onclick', "mwl.addClass('#btn-main-action', 'hide'); mwl.removeClass('#btn-main-action', 'inline'); mwl.setGroupTarget('#flight-result-display', '#flight-date-list', 'show', 'hide')");
		listButton.innerHTML = '&nbsp;';
		col3.appendChild(listButton);
		
		// Toggle sorting mechanism
		var otherSort;
		var sortIcon;
		var sortLabel;
		if (defaultSort == 'priceasc') {
			otherSort = 'departureasc';
			sortIcon = 'time-sort-icon';
			sortLabel = string_price;
		} else {
			otherSort = 'priceasc';
			sortIcon = 'price-sort-icon';
			sortLabel = string_departure_time;
		}
		
		var toggleSortButton = document.createElement('div');
		toggleSortButton.setAttribute('class', 'button-square-small ' + sortIcon);
		toggleSortButton.setAttribute('onclick', TRANSITION_FUNCTION + 'Travelas.sortFlightBy(\'' + otherSort + '\'); ' + refreshFunction);
		toggleSortButton.innerHTML = '&nbsp;';
		col4.appendChild(toggleSortButton);
		
		var row3 = document.createElement('tr');
		breadcrumb.appendChild(row3);
		
		col1 = document.createElement('td');
		col1.innerHTML = '<p>' + string_from + ' IDR ' + numeral(currentDatePrice.price).format('0,0') + '</p>';
		row3.appendChild(col1);
		
		var row4 = document.createElement('tr');
		breadcrumb.appendChild(row4);
		
		col1 = document.createElement('td');
		row4.appendChild(col1);
		col1.setAttribute('colspan', 4);
		col1.innerHTML = '<p>' + string_sorted_by + ' ' + sortLabel + '.</p>';
		
		// Process the flight
		var dataLength = flightSearchResult[listKey].result.length;
		var formattedData = Array();
		if (dataLength > 0) {
			for (var i = 0; i < dataLength; i++) {
				var data = flightSearchResult[listKey].result[i];
				
				var flightId = data.flight_id;
				var flightNumber = data.flight_number;
				var airlineName = data.airlines_name;
				var price = data.price_value;
				var adultPrice = data.price_adult;
				var departureTime = data.simple_departure_time;
				var arrivalTime = data.simple_arrival_time;
				var stop = data.stop;
				var duration = data.duration.replace('j', string_hour_short);
				var image = data.image;
				
				var itemAction = '';
				if (listKey == 'departures') {
					// show return flight list
					itemAction = 'Travelas.chooseDepartFlight(\'' + JSON.stringify(data).replace('"', '\"') + '\')';
				} else {
					itemAction = 'Travelas.chooseReturnFlight(\'' + JSON.stringify(data).replace('"', '\"') + '\')';
				}
				
				formattedData.push({
					image: image,
					action: TRANSITION_FUNCTION + itemAction,
					line1: airlineName + ' (' + flightNumber + ')',
					line2: departureTime + ' - ' + arrivalTime + ' (' + duration + ')',
					line3: '<strong style=\'font-size: medium;\'>' + 'IDR ' + numeral(adultPrice).format('0,0') + '</strong> / ' + string_seat,
					line4: (stop == 'Direct' ? null : '<p>' + stop + '</p>')
				});
			}
		} else {
			formattedData.push({
				action: TRANSITION_FUNCTION + refreshFunction,
				image: 'travelan/img/refresh.png',
				line1: '<strong>' + string_error_no_flight + '</strong>'
			});
		}
		
		// Add ads to flight result list
		var adsContainer = document.createElement('div');
		mainContainer.appendChild(adsContainer);
		adsContainer.setAttribute('id', 'nax-list-ads-inline');
		
		_showInlineAds('nax-list-ads-inline');
		
		var listParent = document.createElement('div');
		mainContainer.appendChild(listParent);
		_generateClickableFlightList(listParent, formattedData);
		
		// Generate date list
		var mainDateContainer = document.getElementById('flight-date-list');
		mainDateContainer.innerHTML = '';
		
		_generateClickableFlightList(mainDateContainer, formattedNearbyDate, dateIndex);
	}
	
	function _verifyDate(dayElem, monthElem, yearElem, comparerDate) {
		var valDay = dayElem.options[dayElem.selectedIndex].value;
		var valMonth = monthElem.options[monthElem.selectedIndex].value;
		var valYear = yearElem.options[yearElem.selectedIndex].value;
		
		comparerDate.setHours(0, 0, 0, 0);
		var selectedDateObject = new Date(valYear, valMonth - 1, valDay);
	
		// set the value of depart date component
		dayElem.value = selectedDateObject.getDate();
		monthElem.value = selectedDateObject.getMonth() + 1;
		yearElem.value = selectedDateObject.getFullYear();
		
		if (selectedDateObject.getTime() < comparerDate.getTime()) {
			return false;
		}
		
		return selectedDateObject;
	}
	
	function _sortFlightBy(otherSort) {
		defaultSort = otherSort;
		_savePreference('sort', otherSort);
	}
	
	function _swapAirports() {
		mwl.show('#all-content-container');
		
		mwl.switchClass('#progress-anim', 'progress-show', 'progress-hide');
		mwl.show('#btn-swap-airport');
		
		departureAirport = document.getElementById('departure-airport');
		arrivalAirport = document.getElementById('arrival-airport');
		
		departureChoice = departureAirport.options[departureAirport.selectedIndex].value;
		arrivalChoice = arrivalAirport.options[arrivalAirport.selectedIndex].value;
		
		departureAirport.value = arrivalChoice;
		arrivalAirport.value = departureChoice;
	}
	
	function _parseDate(dateString) {
		var dateParts = dateString.split('-');
		return new Date(dateParts[0], dateParts[1] - 1, dateParts[2]);
	}
	
	function _showAirportsMap() {
		_setMainAction(TRANSITION_FUNCTION + ' Travelas.backToSearchForm();');
	
		_removePageContent();
		
		mwl.switchClass('#progress-anim', 'progress-show', 'progress-hide');
		mwl.setGroupTarget('#main-content-wrapper', '#all-content-container', 'show', 'hide');
		mwl.setGroupTarget('#all-content-container', '#airport-map-display', 'show', 'hide');
		
		var mapDisplay = document.getElementById('airport-map-display');
		var btnZoomOut = document.getElementById('btn-zoom-out');
		var btnZoomIn = document.getElementById('btn-zoom-in');
		
		btnZoomOut.setAttribute('onclick', TRANSITION_FUNCTION + ' Travelas.setMapZoom(' + (mapZoom - 1) + ')');
		btnZoomIn.setAttribute('onclick', TRANSITION_FUNCTION + ' Travelas.setMapZoom(' + (mapZoom + 1) + ')');
		
		var departureAirport = document.getElementById('departure-airport');
		var arrivalAirport = document.getElementById('arrival-airport');
		
		var departureChoice = departureAirport.options[departureAirport.selectedIndex].value;
		var arrivalChoice = arrivalAirport.options[arrivalAirport.selectedIndex].value;
		
		var departureCity = departureAirport.options[departureAirport.selectedIndex].text;
		var arrivalCity = arrivalAirport.options[arrivalAirport.selectedIndex].text;
		
		// find the airport choice in airport location dataset
		var departureLocation = AIRPORT_LOCATIONS[departureChoice];
		var arrivalLocation = AIRPORT_LOCATIONS[arrivalChoice];
		
		_setHeaderBreadcrumb(string_title_airports_map);
		
		var departureMapContainer = document.getElementById('airport-map-departure');
		var arrivalMapContainer = document.getElementById('airport-map-arrival');
		var allMapContainer = document.getElementById('airport-map-all');

		var mapWidth = window.innerWidth;
		
		_renderAirportMap(departureMapContainer, string_depart_from + departureCity, departureLocation, mapWidth);
		_renderAirportMap(arrivalMapContainer, string_arrive_to + arrivalCity, arrivalLocation, mapWidth);
		_renderAllGeoPoint(allMapContainer, string_all_geo, departureLocation, arrivalLocation, mapWidth);
	}
	
	function _renderAllGeoPoint(parentContainer, titleText, airportLocation1, airportLocation2, mapWidth) {
		parentContainer.innerHTML = '';
		
		var title = document.createElement('div');
		parentContainer.appendChild(title);
		title.setAttribute('class', 'map-title-header');
		title.innerHTML = '<p>' + titleText + '</p>';
		
		if (airportLocation1 && airportLocation2) {
			var distance = _calculateDistance(airportLocation1.lat, airportLocation1['long'], airportLocation2.lat, airportLocation2['long']);
			
			var distanceContainer = document.createElement('div');
			parentContainer.appendChild(distanceContainer);
			distanceContainer.setAttribute('class', 'map-distance-container');
			distanceContainer.innerHTML = '<p>' + string_travel_distance + numeral(distance).format('0,0') + ' km</p>';
			
			var mapUrl = mapBaseUrl + '&w=' + mapWidth 
				+ '&poix0=' + airportLocation1.lat + ',' + airportLocation1['long'] + ';blue;white;11' 
				+ '&poix1=' + airportLocation2.lat + ',' + airportLocation2['long'] + ';blue;white;11';
			
			var image = document.createElement('img');
			parentContainer.appendChild(image);
			image.setAttribute('src', mapUrl);
		} else {
			var distanceContainer = document.createElement('div');
			parentContainer.appendChild(distanceContainer);
			distanceContainer.setAttribute('class', 'map-distance-container');
			distanceContainer.innerHTML = '<p>' + string_error_airport_unknown + '</p>';
		}
	}
	
	function _renderAirportMap(parentContainer, titleText, airportLocation, mapWidth) {
		parentContainer.innerHTML = '';
		
		var title = document.createElement('div');
		parentContainer.appendChild(title);
		title.setAttribute('class', 'map-title-header');
		title.innerHTML = '<p>' + titleText + '</p>';
		
		if (airportLocation) {
			var distanceContainer = document.createElement('div');
			parentContainer.appendChild(distanceContainer);
			distanceContainer.setAttribute('class', 'map-distance-container');
			distanceContainer.innerHTML = '<p>' + string_distance_unknown + '</p>';
			
			if (storage.geolocation != null) {
				var distance = _calculateDistance(storage.geolocation.latitude, storage.geolocation.longitude, airportLocation.lat, airportLocation['long']);
				
				distanceContainer.innerHTML = '<p>' + string_distance_from_airport + numeral(distance).format('0,0') + ' km</p>';
			}
			
			var mapUrl = mapBaseUrl + '&z=' + mapZoom + '&w=' + mapWidth + '&c=' + airportLocation.lat + ',' + airportLocation['long'];
			
			var image = document.createElement('img');
			parentContainer.appendChild(image);
			image.setAttribute('src', mapUrl);
		} else {
			var distanceContainer = document.createElement('div');
			parentContainer.appendChild(distanceContainer);
			distanceContainer.setAttribute('class', 'map-distance-container');
			distanceContainer.innerHTML = '<p>' + string_error_airport_unknown + '</p>';
		}
	}
	
	function _setMapZoom(newZoom) {
		if (newZoom < 1) {
			newZoom = 1;
		} else if (newZoom > 16) {
			newZoom = 16;
		}
		
		mapZoom = newZoom;
		_savePreference('zoom', mapZoom);
		
		_showAirportsMap();
	}
	
	function _calculateDistance(lat1, long1, lat2, long2) {
		var R = 6371;
		
		var dLat = _toRad(lat2 - lat1);
		var dLon = _toRad(long2 - long1);
		var lat1 = _toRad(lat1);
		var lat2 = _toRad(lat2);
		
		var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
		        Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2); 
		var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
		var d = R * c;
		
		return d;
	}
	
	function _toRad(value) {
		return Math.PI * value / 180;
	}
	
	function _savePreference(key, value) {
		if (widget && widget.preferences) {
			widget.preferences.setItem(key, value);
		}
	}
	
	function _getPreference(key, defaultValue) {
		var value = defaultSort;
		if (widget && widget.preferences) {
			var value = widget.preferences.getItem(key);
			
			if (!value) {
				value = defaultValue;
			}
		}
		
		return value;
	}
	
	return {
		init: _initLocation,
		backToSearchForm: _backToSearchForm,
		initiateSearchForm: _initiateSearchForm,
		swapAirports: _swapAirports,
		searchFlight: _searchFlight,
		getNewDepartFlight: _getNewDepartFlight,
		getNewReturnFlight: _getNewReturnFlight,
		chooseDepartFlight: _chooseDepartFlight,
		chooseReturnFlight: _chooseReturnFlight,
		searchFlightByPromo: _searchFlightByPromo,
		bookToPartner: _bookToPartner,
		sortFlightBy: _sortFlightBy,
		showAirportsMap: _showAirportsMap,
		setMapZoom: _setMapZoom,
		calculateDistance: _calculateDistance
	};
}());