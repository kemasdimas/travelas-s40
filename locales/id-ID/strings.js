var string_tab_search = 'Cari Tiket';
var string_tab_promo = 'Promo';

var string_share_message = '{airline} {from} ke {to} tgl {date}, IDR {price} /kursi. Punya HP Nokia? Dapatkan tiket murah di {appurl}';

var string_error_airports_no_choice = 'Silakan pilih bandara berangkat &amp; pulang.';
var string_error_airports_same = 'Bandara pulang &amp; pergi harus berbeda.';
var string_error_invalid_departure_date = 'Tanggal keberangkatan tidak tepat, harus lebih dari ';
var string_error_invalid_return_date = 'Tanggal pulang tidak tepat, harus lebih dari ';
var string_error_too_many_infant = 'Jumlah bayi dalam perjalanan tidak boleh lebih dari jumlah orang dewasa.';
var string_error_server_problem = 'Ma\'af sedang ada masalah pada server parter kami, silakan coba kembali.';
var string_error_no_flight = 'Sementara tidak dapat menemukan penerbangan, silakan coba lagi.';
var string_error_airport_unknown = 'Tidak dapat menemukan bandara ini.';

var string_loading_progress = '&nbsp;&nbsp;Harap tunggu...';
var string_departure_airport = 'Bandara Keberangkatan';
var string_arrival_airport = 'Bandara Kedatangan';
var string_departure_hint = 'Pilih ' + string_departure_airport;
var string_arrival_hint = 'Pilih ' + string_arrival_airport;
var string_round_trip_flight = 'Penerbangan Pulang Pergi?';
var string_departure_date = 'Tanggal Pergi';
var string_return_date = 'Tanggal Pulang';
var string_day = 'Tanggal';
var string_month = 'Bulan';
var string_year = 'Tahun';
var string_passengers = 'Penumpang';
var string_adult = 'Dewasa';
var string_adults = string_adult;
var string_child = 'Anak';
var string_childs = string_child;
var string_infant = 'Bayi';
var string_infants = string_infant;
var string_yrs = 'thn';
var string_promo_price_change = 'Harga promo dapat berubah sewaktu-waktu.';
var string_to = 'ke';
var string_from = 'dari';
var string_hour_short = 'j';
var string_minute_short = 'm';
var string_back_to_top = 'Kembali ke Atas';
var string_search_flights = 'Cari Penerbangan';

var string_title_airports_map = 'Peta Lokasi Bandara';
var string_zoom_out_map = 'Perkecil Peta';
var string_zoom_in_map = 'Perbesar Peta';
var string_depart_from = 'Berangkat dari ';
var string_all_geo = 'Peta Perjalanan';
var string_travel_distance = 'Jarak tempuh perjalanan: ';
var string_arrive_to = 'Datang ke ';
var string_distance_from_airport = 'Jarak Anda ke bandara: ';
var string_distance_unknown = 'Ma\'af kami tidak bisa mendapatkan lokasi Anda.';
var string_location_load = '<strong>Mencari bandara terdekat...</strong><br/>Izinkan kami menggunakan data lokasi Anda.';

var string_title_departure_flight = 'Pilih Penerbangan Pergi';
var string_title_return_flight = 'Pilih Penerbangan Pulang';
var string_sorted_by = 'Urut berdasarkan';
var string_price = 'harga';
var string_departure_time = 'waktu berangkat';
var string_starts = 'Dimulai';
var string_seat = 'kursi';

var string_title_flight_detail = 'Detail Penerbangan';
var string_skip_ads = 'Abaikan Iklan';
var string_departure_flight = 'Penerbangan Pergi';
var string_return_flight = 'Penerbangan Pulang';
var string_share_to_friends = 'Beri tahu kawan';
var string_exclude_tax = 'Belum termasuk pajak &amp; biaya layanan.';
var string_book_flight = 'Pesan Tiket via Telepon';

//Addition in left menu button
var string_customer_service = 'Layanan Pelanggan';
var string_about = 'Tentang Travelas S40';
var string_back_to_main = 'Kembali ke Halaman Utama';
var string_contact_call = 'Telepon Tiket.com';
var string_cs_parag_1 = 'Ada pertanyaan seputar booking pesawat di aplikasi Travelas S40? Hubungi mitra perjalanan kami Tiket.com untuk informasi booking tiket.';
var string_about_parag_1 = 'Cari dan pesan tiket penerbangan murah dengan Travelas S40! Dengan dukungan maskapai Garuda Indonesia, Citilink, Lion Air, Sriwijaya Air dan Merparti.';
var string_about_parag_2 = '<strong>Data dan booking pesawat dioperasikan oleh Tiket.com</strong>';