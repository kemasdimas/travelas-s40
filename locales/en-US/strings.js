var string_tab_search = 'Flight Search';
var string_tab_promo = 'Promo';

var string_share_message = '{airline} {from} - {to}, {date}, IDR {price}/seat. Using Nokia S40? Get cheap flight from {appurl}';

var string_error_airports_no_choice = 'Please choose departure &amp; arrival airport.';
var string_error_airports_same = 'Arrival &amp; departure airport must be different.';
var string_error_invalid_departure_date = 'Invalid departure date, must not before ';
var string_error_invalid_return_date = 'Invalid return date, must not before ';
var string_error_too_many_infant = 'Total of infant passengers must be less or equal to adult passengers.';
var string_error_server_problem = 'Sorry, there\'s a problem with our partner server, please try again.';
var string_error_no_flight = 'Temporary unable to find any flight, please click refresh button.';
var string_error_airport_unknown = 'Sorry, unable to locate this airport';

var string_loading_progress = '&nbsp;&nbsp;Loading...';
var string_departure_airport = 'Departure Airport';
var string_arrival_airport = 'Arrival Airport';
var string_departure_hint = 'Choose departure airport';
var string_arrival_hint = 'Choose arrival airport';
var string_round_trip_flight = 'Round Trip Flight?';
var string_departure_date = 'Departure Date';
var string_return_date = 'Return Date';
var string_day = 'Day';
var string_month = 'Month';
var string_year = 'Year';
var string_passengers = 'Passengers';
var string_adult = 'Adult';
var string_adults = 'Adult(s)';
var string_child = 'Child';
var string_childs = 'Child(s)';
var string_infant = 'Infant';
var string_infants = 'Infant(s)';
var string_yrs = 'yrs';
var string_promo_price_change = 'Promo price can be changed anytime without notice.';
var string_to = 'to';
var string_from = 'from';
var string_hour_short = 'h';
var string_minute_short = 'm';
var string_back_to_top = 'Back To Top';
var string_search_flights = 'Search Flights';

var string_title_airports_map = 'Selected Airports Map';
var string_zoom_out_map = 'Zoom Out Map';
var string_zoom_in_map = 'Zoom In Map';
var string_depart_from = 'Depart from ';
var string_all_geo = 'Overview Map';
var string_travel_distance = 'Travel distance: ';
var string_arrive_to = 'Arrive to ';
var string_distance_from_airport = 'Your distance from airport: ';
var string_distance_unknown = 'Sorry, We can not get your current location.';
var string_location_load = '<strong>Finding nearest airport...</strong><br/>Please allow us to use location data.';

var string_title_departure_flight = 'Choose Departure Flight';
var string_title_return_flight = 'Choose Return Flight';
var string_sorted_by = 'Sorted by';
var string_price = 'price';
var string_departure_time ='departure time';
var string_starts = 'Starts';
var string_seat = 'seat';

var string_title_flight_detail = 'Flights Detail';
var string_skip_ads = 'Skip Ads';
var string_departure_flight = 'Departure Flight';
var string_return_flight = 'Return Flight';
var string_share_to_friends = 'Share to friends';
var string_exclude_tax = 'Excluding tax &amp; service fee.';
var string_book_flight = 'Book Flight via Phone';

// Addition in left menu button
var string_customer_service = 'Customer Service';
var string_about = 'About';
var string_back_to_main = 'Back To Main Page';
var string_contact_call = 'Call Tiket.com';
var string_cs_parag_1 = 'Having an issue or question about flight booking in Travelas S40? You can contact our travel partner Tiket.com for assistance.';
var string_about_parag_1 = 'Find cheapest flight in Indonesia using Travelas S40! We support flight search for Garuda Indonesia, Citilink, Lion Air, Sriwijaya Air and Merparti.';
var string_about_parag_2 = '<strong>Flight data and booking is operated by Tiket.com</strong>';